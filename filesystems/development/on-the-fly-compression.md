# On the fly compression

References for designing a hybrid filesystem supporting on the fly compression

# wikipedia

https://en.wikipedia.org/wiki/LZ4_(compression_algorithm)
https://en.wikipedia.org/wiki/Entropy_encoding
https://en.wikipedia.org/wiki/Huffman_coding
https://en.wikipedia.org/wiki/Arithmetic_coding
https://en.wikipedia.org/wiki/Entropy_encoding
https://btrfs.wiki.kernel.org/index.php/Compression

# books
http://www.nobius.org/dbg/practical-file-system-design.pdf
UNIX Filesystems: Evolution, Design, and Implementation
